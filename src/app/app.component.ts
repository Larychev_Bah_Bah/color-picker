import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Color-Picker!';
    public table = [];
    savedTable = [];
    //array of pallete
    public colors = [
        {name: 'red', color: 'rgb(255,0,0)'},
        {name: 'green', color: 'rgb(0,128,0)'},
        {name: 'blue', color: 'rgb(0,0,255)'},
        {name: 'black', color: 'rgb(0,0,0)'}
    ]

    ngOnInit() {
        this.createTable()
    }

    //Checking for blending colors
    mixColor($event, item) {
        if (!item.wasEdit) {
            item.color = $event.dragData.color
            item.wasEdit = 1
            console.log(this.table)
        }
        else {
            let numberPattern = /\d+/g;
            let convertedColorOld = item.color.match(numberPattern)
            let convertedColorOldObj = {
                r: convertedColorOld[0],
                g: convertedColorOld[1],
                b: convertedColorOld[2]
            };
            let convertedColorNew = $event.dragData.color.match(numberPattern)
            let convertedColorNewObj = {
                r: convertedColorNew[0],
                g: convertedColorNew[1],
                b: convertedColorNew[2]
            }
            let newColor = 'rgb(' + Math.round(Math.abs(convertedColorOldObj.r - convertedColorNewObj.r) / 2) + ',' +
                Math.round(Math.abs(convertedColorOldObj.g - convertedColorNewObj.g) / 2) + ',' +
                Math.round(Math.abs(convertedColorOldObj.b - convertedColorNewObj.b) / 2) + ')'
            item.color = newColor
        }
        this.getArray()
    }


    //Create array of objects 10*10 for grid
    createTable() {
        let arr = [],
            length = 100
        for (let i = 0; i < length; i++) {
            arr.push({
                name: 'Default',
                color: 'rgb(192,192,192)',
                index: i,
                wasEdit: false
            });
        }
        return this.table = arr
    }

    //Setting color of cell
    setColor($event, item) {
        this.mixColor($event, item)
    }

    //way to show the JSON;
    getArray() {
        let newArray = this.table.slice();
        let myJsonString = JSON.stringify(newArray);
        return myJsonString
    }

    //Save Grid, starts from button
    saveTable() {
        let clonedArray = JSON.parse(JSON.stringify(this.table))
        this.savedTable = clonedArray
    }

    //Load Grid, starts from button
    loadTable() {
        this.createTable()
        let clonedArray = JSON.parse(JSON.stringify(this.savedTable))
        this.table = clonedArray
    }

}
